<?php

namespace Printer;

use Mike42\Escpos\PrintConnectors\PrintConnector;

class Bridge
{
    protected $connector;

    protected $bufferTransport;

    public function __construct(BufferTransport $bufferTransport)
    {
        $this->bufferTransport = $bufferTransport;
    }

    public function connect(PrintConnector $connector)
    {
        $this->connector = $connector;
    }

    public function print(string $string): void
    {
        $data = $this->bufferTransport->parse($string);

        $this->connector->write($data);
        $this->connector->finalize();
    }
}
