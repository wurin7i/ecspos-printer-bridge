<?php

namespace Printer;

use Mike42\Escpos\PrintConnectors\DummyPrintConnector;

class BufferTransport
{
    const DELIMITER = ':';

    /**
     * Get print buffer text with checksum
     *
     * @param \Mike42\Escpos\PrintConnectors\DummyPrintConnector $data
     * @param string|null $delimiter
     * @return string
     */
    public final function stringify(DummyPrintConnector $connector, ?string $delimiter = null): string
    {
        $data = $connector->getData();
        $delimiter = $delimiter ?: self::DELIMITER;
        $dataString = $this->encode($data);
        $checksum = $this->checksum($dataString);

        return implode($delimiter, [$dataString, $checksum]);
    }

    public final function parse(string $token, ?string $delimiter = null): ?string
    {
        $delimiter = $delimiter ?: self::DELIMITER;

        if (! preg_match("~^(.*){$delimiter}([a-f0-9]{32})$~", $token, $matches)) {
            return null;
        }

        [, $dataString, $checksum] = $matches;
        $data = $this->decode($dataString);

        if ($this->checksum($checksum, $data)) {
            return $data;
        }
    }

    protected function encode($data)
    {
        $data = is_array($data) ? implode($data) : $data;

        return base64_encode($data);
    }

    public function decode($data)
    {
        return base64_decode($data);
    }

    protected function verify(string $hash, string $data): bool
    {
        return $hash === $this->checksum($data);
    }

    protected function checksum(string $data): string
    {
        return md5($data);
    }
}