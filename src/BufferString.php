<?php

namespace Printer;

/**
 * Deprecated. Use BufferTransport instead.
 * @deprecated v0.1
 */
class BufferString
{
    const DELIMITER = ':';

    /**
     * Get print buffer text with checksum
     *
     * @param mixed $data
     * @param string|null $delimiter
     * @return string
     */
    public final function stringify($data, ?string $delimiter = null): string
    {
        $delimiter = $delimiter ?: self::DELIMITER;
        $dataString = $this->encode($data);
        $checksum = $this->checksum($dataString);

        return implode(self::DELIMITER, [$checksum, $dataString]);
    }

    public final function parse(string $dataString, ?string $delimiter = null): ?string
    {
        $delimiter = $delimiter ?: self::DELIMITER;
        $matches = explode($delimiter, $dataString, 2);

        if (count($matches) < 2) {
            return null;
        }

        $data = $this->decode($matches[3]);
        if ($this->checksum($matches[2], $data)) {
            return $data;
        }
    }

    protected function encode($data)
    {
        $data = is_array($data) ? implode($data) : $data;

        return base64_encode($data);
    }

    public function decode($data)
    {
        return base64_decode($data);
    }

    protected function verify(string $hash, string $data): bool
    {
        return $hash === $this->checksum($data);
    }

    protected function checksum(string $data): string
    {
        return md5($data);
    }
}